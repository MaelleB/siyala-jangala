// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickableDoodad.h"
#include "PickableScalingFruit.generated.h"

/**
 * 
 */
UCLASS()
class SIYALAJANGALA_API APickableScalingFruit : public APickableDoodad
{
	GENERATED_BODY()
	
public:
	APickableScalingFruit();

	virtual void Tick(float DeltaTime) override;

	virtual void PickUp() override;

protected:
	virtual void BeginPlay() override;

	FVector Scale;
};
