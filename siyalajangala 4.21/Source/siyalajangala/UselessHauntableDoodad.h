// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HauntableDoodad.h"
#include "UselessHauntableDoodad.generated.h"

UCLASS()
class SIYALAJANGALA_API AUselessHauntableDoodad : public AHauntableDoodad
{
	GENERATED_BODY()

public:
	AUselessHauntableDoodad();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Action();

	void MoveForward(float value);
	void MoveRight(float value);
};
