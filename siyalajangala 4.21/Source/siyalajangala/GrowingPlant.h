#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "GrowingPlant.generated.h"

class APickableDoodad;
class AHauntableDoodad;

UCLASS()
class SIYALAJANGALA_API AGrowingPlant : public AActor {
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrowingPlant();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool bSeed;
	bool bWater;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* EarthMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* SeedMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* WaterMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlantMesh1;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlantMesh2;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlantMesh3;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlantMesh4;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlantMesh5;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void PlaceSeed(APickableDoodad* Seed);
	void PlaceWater(AHauntableDoodad* Water);

	void GrowPlant();
};
