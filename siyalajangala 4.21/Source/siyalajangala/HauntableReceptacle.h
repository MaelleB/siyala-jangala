// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HauntableDoodad.h"
#include "Components/StaticMeshComponent.h"
#include "HauntableReceptacle.generated.h"

class APickableDoodad;

UCLASS()
class SIYALAJANGALA_API AHauntableReceptacle : public AHauntableDoodad
{
	GENERATED_BODY()
	
public:
	AHauntableReceptacle();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BaseMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FragmentMesh1;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FragmentMesh2;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FragmentMesh3;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* KeyMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh;

public:
	void Place(APickableDoodad* Item);
	virtual void Tick(float DeltaTime) override;
	virtual void Action();
};
