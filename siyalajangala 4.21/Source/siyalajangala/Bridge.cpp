#include "Bridge.h"
#include "HauntableDoodad.h"
#include "HauntablePlank.h"

// Sets default values
ABridge::ABridge() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BridgeMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Bridge Mesh"));
	RootComponent = BridgeMesh;

	PlankMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank Mesh 1"));
	PlankMesh1->SetupAttachment(RootComponent);
	PlankMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank Mesh 2"));
	PlankMesh2->SetupAttachment(RootComponent);
	PlankMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank Mesh 3"));
	PlankMesh3->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABridge::BeginPlay() {
	AActor::BeginPlay();

	PlankMesh1->SetVisibility(false);
	PlankMesh1->SetSimulatePhysics(false);
	PlankMesh1->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlankMesh2->SetVisibility(false);
	PlankMesh2->SetSimulatePhysics(false);
	PlankMesh2->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlankMesh3->SetVisibility(false);
	PlankMesh3->SetSimulatePhysics(false);
	PlankMesh3->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called every frame
void ABridge::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);

}

void ABridge::PlacePlank(AHauntableDoodad* Plank) {
	bool isPlank = Plank->GetClass()->IsChildOf(AHauntablePlank::StaticClass());
	
	if (isPlank) {
		if (!PlankMesh1->IsVisible()) {
			PlankMesh1->SetVisibility(true);
			PlankMesh1->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		}
		else if (!PlankMesh2->IsVisible()) {
			PlankMesh2->SetVisibility(true);
			PlankMesh2->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		} else {
			PlankMesh3->SetVisibility(true);
			PlankMesh3->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		}

		Plank->Destroy();
	}
}
