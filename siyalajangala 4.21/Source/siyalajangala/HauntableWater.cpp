#include "HauntableWater.h"
#include "GrowingPlant.h"
#include "SJ_PlayerController.h"
#include "DrawDebugHelpers.h"

AHauntableWater::AHauntableWater() : AHauntableDoodad() {
	maxHeight = GetActorLocation().Z + 1000;
	minHeight = GetActorLocation().Z;
	CurrentItem = nullptr;
	CameraInput.X = 0;
	CameraInput.Y = 0;
}

void AHauntableWater::BeginPlay() {
	Super::BeginPlay();
}

void AHauntableWater::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bPossessed) {
		//Rotate our camera's pitch, but limit it so we're always looking downward
		{
			FRotator NewRotation = SpringArm->GetComponentRotation();
			NewRotation.Pitch = FMath::Clamp(NewRotation.Pitch + CameraInput.X, -80.0f, 80.0f);
			SpringArm->SetWorldRotation(NewRotation);
		}

		if (CameraInput.Y != 0) {
			FVector Axis(0, 0, 1);
			AddActorLocalRotation(FQuat(Axis, CameraInput.Y * DeltaTime));
		}

		{
			if (!MovementInput.IsZero()) {
				//Scale our movement input axis values by 300 units per second
				MovementInput = MovementInput.GetSafeNormal() * 500.0f;
				FVector NewLocation = GetActorLocation();
				NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
				NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
				SetActorLocation(NewLocation);
			}
		}

		DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

		Start = Camera->GetComponentLocation();
		ForwardVector = Camera->GetForwardVector();
		End = (ForwardVector * 2000.0f) + Start;

		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(AGrowingPlant::StaticClass())) {
				CurrentItem = Cast<AGrowingPlant>(Hit.GetActor());
			} else {
				CurrentItem = nullptr;
			}
		} else {
			CurrentItem = nullptr;
		}
	}
}

void AHauntableWater::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	APawn::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntableWater::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AHauntableWater::Action);

	PlayerInputComponent->BindAxis("MoveForward", this, &AHauntableWater::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHauntableWater::MoveRight);

	PlayerInputComponent->BindAction("GoUp", IE_Pressed, this, &AHauntableWater::GoUp);
	PlayerInputComponent->BindAction("GoDown", IE_Pressed, this, &AHauntableWater::GoDown);

	PlayerInputComponent->BindAxis("Turn", this, &AHauntableWater::YawCamera);
	PlayerInputComponent->BindAxis("LookUp", this, &AHauntableWater::PitchCamera);
}

void AHauntableWater::MoveForward(float AxisValue) {
	MovementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AHauntableWater::MoveRight(float AxisValue) {
	MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AHauntableWater::PitchCamera(float AxisValue) {
	CameraInput.X = -1 * AxisValue;
}

void AHauntableWater::YawCamera(float AxisValue) {
	CameraInput.Y = AxisValue;
}

void AHauntableWater::GoUp() {
	FVector NewLocation = GetActorLocation();
	NewLocation += GetActorUpVector() * 100;
	if (NewLocation.Z >= maxHeight) {
		NewLocation.Z = maxHeight;
	}
	SetActorLocation(NewLocation);
}

void AHauntableWater::GoDown() {
	FVector NewLocation = GetActorLocation();
	NewLocation += GetActorUpVector() * -100;
	if (NewLocation.Z <= minHeight) {
		NewLocation.Z = minHeight;
	}
	SetActorLocation(NewLocation);
}


void AHauntableWater::Action() {
	if (CurrentItem != nullptr) {
		Unpossess();
		CurrentItem->PlaceWater(this);
	}
}

void AHauntableWater::Unpossess() {
	AHauntableDoodad::Unpossess();
	MovementInput = FVector2D(0, 0);
}