// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ConcreteDoodad.h"
#include "CorruptedDoodad.generated.h"

/**
 *
 */
UCLASS()
class SIYALAJANGALA_API ACorruptedDoodad : public AConcreteDoodad {
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	ACorruptedDoodad();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
};
