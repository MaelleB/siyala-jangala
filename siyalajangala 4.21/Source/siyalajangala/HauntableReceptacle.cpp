// Fill out your copyright notice in the Description page of Project Settings.

#include "HauntableReceptacle.h"
#include "PickableDoodad.h"
#include "PickableFragment.h"
#include "PickableKey.h"

AHauntableReceptacle::AHauntableReceptacle() {
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
	RootComponent = BaseMesh;

	FragmentMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fragment 1"));
	FragmentMesh1->SetupAttachment(RootComponent);

	FragmentMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fragment 2"));
	FragmentMesh2->SetupAttachment(RootComponent);

	FragmentMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fragment 3"));
	FragmentMesh3->SetupAttachment(RootComponent);

	KeyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Key Mesh"));
	KeyMesh->SetupAttachment(RootComponent);

	StairwayToHeavenMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs"));
	StairwayToHeavenMesh->SetupAttachment(RootComponent);
}

void AHauntableReceptacle::BeginPlay() {
	BaseMesh->SetVisibility(true);
	BaseMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	FragmentMesh1->SetVisibility(false);
	FragmentMesh1->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	FragmentMesh2->SetVisibility(false);
	FragmentMesh2->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	FragmentMesh3->SetVisibility(false);
	FragmentMesh3->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	KeyMesh->SetVisibility(false);
	KeyMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StairwayToHeavenMesh->SetVisibility(false);
	StairwayToHeavenMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AHauntableReceptacle::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);
}

void AHauntableReceptacle::Place(APickableDoodad* Item) {
	if (Item->GetClass()->IsChildOf(APickableKey::StaticClass())) {
		KeyMesh->SetVisibility(true);
		KeyMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	} else if (Item->GetClass()->IsChildOf(APickableFragment::StaticClass())) {
		if (!FragmentMesh1->IsVisible()) {
			FragmentMesh1->SetVisibility(true);
			FragmentMesh1->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		} else if (!FragmentMesh2->IsVisible()) {
			FragmentMesh2->SetVisibility(true);
			FragmentMesh2->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		} else if (!FragmentMesh3->IsVisible()) {
			FragmentMesh3->SetVisibility(true);
			FragmentMesh3->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		}
	}
}

void AHauntableReceptacle::Action() {
	if (KeyMesh->IsVisible() && FragmentMesh3->IsVisible()) {
		StairwayToHeavenMesh->SetVisibility(true);
		StairwayToHeavenMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}
}