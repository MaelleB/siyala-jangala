// Fill out your copyright notice in the Description page of Project Settings.

#include "HauntableMovable.h"

AHauntableMovable::AHauntableMovable() : AHauntableDoodad() {}

void AHauntableMovable::BeginPlay() {
	Super::BeginPlay();
}

void AHauntableMovable::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (!MovementInput.IsZero()) {
		//Scale our movement input axis values by 300 units per second
		MovementInput = MovementInput.GetSafeNormal() * 500.0f;
		FVector NewLocation = GetActorLocation();
		NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
		NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
		SetActorLocation(NewLocation);
	}

}

void AHauntableMovable::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	APawn::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Turn", this, &AHauntableMovable::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AHauntableMovable::AddControllerPitchInput);

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntableMovable::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AHauntableMovable::Action);

	PlayerInputComponent->BindAxis("MoveForward", this, &AHauntableMovable::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHauntableMovable::MoveRight);
}

void AHauntableMovable::Action() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Possessing Movable !"));
	}
}

void AHauntableMovable::MoveForward(float Value) {
	MovementInput.X = FMath::Clamp<float>(Value, -1.0f, 1.0f);
}

void AHauntableMovable::MoveRight(float Value) {
	MovementInput.Y = FMath::Clamp<float>(Value, -1.0f, 1.0f);
}

void AHauntableMovable::Unpossess() {
	Super::Unpossess();
	MovementInput = FVector2D(0, 0);
}