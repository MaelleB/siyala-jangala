#pragma once

#include "CoreMinimal.h"
#include "PickableDoodad.h"
#include "SmellableObject.h"
#include "PickableSeed.generated.h"

UCLASS()
class SIYALAJANGALA_API APickableSeed : public APickableDoodad, public ISmellableObject {
	GENERATED_BODY()
};
