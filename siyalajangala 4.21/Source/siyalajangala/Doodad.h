#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Doodad.generated.h"

UENUM()
enum class ECorruptionState : uint8
{
	CS_None UMETA(DisplayName="None"),
	CS_Weak UMETA(DisplayName="Weak"),
	CS_Strong UMETA(DisplayName="Strong"),
	CS_Total UMETA(DisplayName="Total")
};

UINTERFACE()
class SIYALAJANGALA_API UDoodad : public UInterface
{
	GENERATED_BODY()
};
class SIYALAJANGALA_API IDoodad
{
	GENERATED_BODY()
public:
	IDoodad();
	IDoodad(ECorruptionState c_state);

public:
	virtual ECorruptionState getCorruption() const;
	virtual void setCorruption(ECorruptionState c_state);
};
