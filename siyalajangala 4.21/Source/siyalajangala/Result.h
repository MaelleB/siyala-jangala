#pragma once

#include <stdexcept>

// Class to ensure that the user are not working with unexistant value.
// T must own a copy constructor
template<class T>
class Result
{
public:
	static Result<T> Some(const T& element) {
		return Result(element);
	}
	static Result<T> None(const std::string& reason) {
		Result r;
		r._reason += reason;
		return r;
	}
	static Result<T> None() {
		return None(" No further reason given by the emitter.");
	}

	bool isUsable() {
		return _object != nullptr;
	}

	T& get() {
		if (!isUsable()) {
			throw std::logic_error(_reason);
		}
		else {
			return *_object;
		}
	}

		~Result() {
		if (_object != nullptr) {
			delete _object;
		}
	}
private:
	Result() : _object(nullptr), _reason("None results access not allowed.") {}
	Result(const T& object) : _object(new T(object)) {}
	T* _object;
	std::string _reason;
};


