#include "Brasero.h"
#include "HauntableLantern.h"

#include <string>

// Sets default values
ABrasero::ABrasero() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BraseroMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Brasero Mesh"));
	RootComponent = BraseroMesh;

	Particles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particles"));
	Particles->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABrasero::BeginPlay() {
	AActor::BeginPlay();
	Particles->Deactivate();
	Particles->InitializeSystem();
	Particles->InitParticles();
}

// Called every frame
void ABrasero::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);

	if (IsLanternNearby()) {
		Particles->Activate();
	}
}


bool ABrasero::IsLanternNearby() {
	float Distance = FVector::Distance(this->GetActorLocation(), Lantern->GetActorLocation());
	return Distance < LanternDistance;
}
