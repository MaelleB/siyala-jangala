#pragma once

#include "CoreMinimal.h"
#include "HauntableDoodad.h"
#include "GameFramework/Character.h"
#include "HauntableWater.generated.h"

class AGrowingPlant;

UCLASS()
class SIYALAJANGALA_API AHauntableWater : public AHauntableDoodad {
	GENERATED_BODY()
	
public:
	AHauntableWater();

protected:
	virtual void BeginPlay() override;

	FVector2D MovementInput;
	FVector2D CameraInput;

	FVector Start;
	FVector ForwardVector;
	FVector End;
	FHitResult Hit;
	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParams;

	AGrowingPlant* CurrentItem;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		float minHeight;
	UPROPERTY(EditAnywhere, Category = Gameplay)
		float maxHeight;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void PitchCamera(float AxisValue);
	void YawCamera(float AxisValue);
	void GoUp();
	void GoDown();

	virtual void Action() override;
	virtual void Unpossess() override;
};
