// Fill out your copyright notice in the Description page of Project Settings.


#include "ConcreteDoodad.h"


// Sets default values
AConcreteDoodad::AConcreteDoodad() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	corruptionState = ECorruptionState::CS_None;
	
	// Both lines below are necessary to see movement replication
	bReplicates = true;
	bReplicateMovement = true;
}

// Called when the game starts or when spawned
void AConcreteDoodad::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AConcreteDoodad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

ECorruptionState AConcreteDoodad::getCorruption() const {
	return corruptionState;
}

void AConcreteDoodad::setCorruption(ECorruptionState c_state) {
	corruptionState = c_state;
}
