// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineIdentityInterface.h"
#include "OnlineSessionInterface.h"
#include "Engine/NetworkDelegates.h"
#include "Result.h"
#include "Utils.h"
#include "FindSessionsCallbackProxy.h"
#include "SJGameInstance.generated.h"

USTRUCT(BlueprintType)
struct FSJSessionResult {

	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(BlueprintReadOnly)
	FBlueprintSessionResult Result;
	
	UPROPERTY(BlueprintReadOnly)
	FString CharacterAvailable;

	UPROPERTY(BlueprintReadOnly)
	FString SessionName;

	UPROPERTY(BlueprintReadOnly)
	int32 SessionIndexInSearchResults;
};

UENUM(BlueprintType)
enum class SJEPlayerType: uint8 {
	Jhada	UMETA(DisplayName="Jhada"),
	Spirit	UMETA(DisplayName="Spirit")
};

FString GetPlayerTypeEnumAsString(SJEPlayerType EnumValue);

/**
 * Based on tutorial that can be found at https://wiki.unrealengine.com/How_To_Use_Sessions_In_C%2B%2B
 */
UCLASS()
class SIYALAJANGALA_API USJGameInstance : public UGameInstance
{
	GENERATED_UCLASS_BODY()

public:
	static FString GetPlayerTypeAsString(SJEPlayerType EnumValue);
	static SJEPlayerType GetPlayerTypeFromString(const FString& EnumName, const FString& String);

	// USJGameInstance(const FObjectInitializer& ObjectInitializer);

	/**
	*	Function to host a game!
	*
	*	@Param		UserID			User that started the request
	*	@Param		SessionName		Name of the Session
	*	@Param		bIsLAN			Is this is LAN Game?
	*	@Param		bIsPresence		"Is the Session to create a presence Session"
	*	@Param		MaxNumPlayers	Number of Maximum allowed players on this "Session" (Server)
	*	@Param		PlayerType		The player type that will be available for the joiner.
	*/
	bool HostSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers);

	/**
	*	Function fired when a session create request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	virtual void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);

	/**
	*	Function fired when a session start request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnStartOnlineGameComplete(FName SessionName, bool bWasSuccessful);

	/**
	*	Find an online session
	*
	*	@param UserId user that initiated the request
	*	@param bIsLAN are we searching LAN matches
	*	@param bIsPresence are we searching presence sessions
	*/
	void FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence);

	/**
	*	Delegate fired when a session search query has completed
	*
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnFindSessionsComplete(bool bWasSuccessful);
	
	bool JoinSession(ULocalPlayer * LocalPlayer, int32 SessionIndexInSearchResults);

	/**
	*	Joins a session via a search result
	*
	*	@param SessionName name of session // Party or Game necessary parameter for UE - not the actual name of the session
	*	@param SearchResult Session to join
	*
	*	@return bool true if successful, false otherwise
	*/
	// bool JoinSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);

	/**
	*	Delegate fired when a session join request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);


	/**
	*	Delegate fired when a destroying an online session has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	virtual void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

	// BLUEPRINTS CALLABLE FUNCTIONS

	UFUNCTION(BlueprintCallable, Category = "Network|Test")
	void StartOnlineGame(bool bUseLan);

	UFUNCTION(BlueprintCallable, Category = "Network|Test")
	void FindOnlineGames(bool bUseLan);

	//UFUNCTION(BlueprintCallable, Category = "Network|Test")
	//void JoinOnlineGame();

	UFUNCTION(BlueprintCallable, Category = "Network|Test")
	bool JoinOnlineGame(FSJSessionResult Result);

	UFUNCTION(BlueprintCallable, Category = "Network|Test")
	void DestroySessionAndLeaveGame();

	
	UFUNCTION(BlueprintCallable, Category = "Network|Test")
	void GetSearchResults(bool& Usable, FString& State, TArray<FSJSessionResult>& SearchResults);
	
	public: // Events
	UFUNCTION(BlueprintImplementableEvent, Category = "Network|Test")
	void OnFindSessionsCompleted(bool bWasSuccessful);

	UFUNCTION(BlueprintImplementableEvent, Category = "Network|Test")
	void OnJoinSessionCompleted(bool bWasSuccessful);

	UFUNCTION(BlueprintImplementableEvent, Category = "Network|Test")
	void OnStartSessionCompleted(bool bWasSuccessful);

	// Blueprints variables

	UPROPERTY(BlueprintReadWrite)
	SJEPlayerType AvailablePlayerType = SJEPlayerType::Jhada;

	UPROPERTY(BlueprintReadWrite)
	FString CustomSessionName;

	UPROPERTY(BlueprintReadOnly)
	FString LastErrorReason;

protected:

	/* Delegate called when session created */
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;
	/* Delegate called when session started */
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;

	/** Handles to registered delegates for creating/starting a session */
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;

	TSharedPtr<class FOnlineSessionSettings> SessionSettings;

	/** Delegate for searching for sessions */
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;

	/** Handle to registered delegate for searching a session */
	FDelegateHandle OnFindSessionsCompleteDelegateHandle;

	// Search results
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	/** Delegate for joining a session */
	FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;

	/** Handle to registered delegate for joining a session */
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;

	/** Delegate for destroying a session */
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;

	/** Handle to registered delegate for destroying a session */
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;
};
