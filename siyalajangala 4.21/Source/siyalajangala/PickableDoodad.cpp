#include "PickableDoodad.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"

APickableDoodad::APickableDoodad() : AConcreteDoodad() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DoodadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoodadMesh"));
	DoodadMesh->SetSimulatePhysics(true);
	RootComponent = Cast<USceneComponent>(DoodadMesh);

	bHolding = false;
	bGravity = true;
}

void APickableDoodad::BeginPlay()
{
	Super::BeginPlay();

	Jhada = UGameplayStatics::GetPlayerCharacter(this, 0);
	PlayerCamera = Jhada->FindComponentByClass<UCameraComponent>();

	TArray<USceneComponent*> Components;
	Jhada->GetComponents(Components);

	if (Components.Num() > 0) {
		for (auto& Comp : Components) {
			if (Comp->GetName() == "HoldingComponent") {
				HoldingComp = Cast<USceneComponent>(Comp);
				FRotator Rotation = HoldingComp->GetComponentRotation();
				Rotation.Yaw += 240;
				HoldingComp->SetRelativeRotation(Rotation);
			}
		}
	}
}

// Called every frame
void APickableDoodad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bHolding && HoldingComp) {
		SetActorLocationAndRotation(HoldingComp->GetComponentLocation() + PlayerCamera->GetForwardVector(), HoldingComp->GetComponentRotation());
	}
}

void APickableDoodad::PickUp() {
	bHolding = !bHolding;
	bGravity = !bGravity;
	DoodadMesh->SetEnableGravity(bGravity);
	DoodadMesh->SetSimulatePhysics(bHolding ? false : true);
	DoodadMesh->SetCollisionEnabled(bHolding ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryAndPhysics);

	if (!bHolding) {
		ForwardVector = PlayerCamera->GetForwardVector();
		DoodadMesh->AddForce(ForwardVector * 10000 * DoodadMesh->GetMass());
	}
}