#include "Jhada.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "PickableDoodad.h"
#include "PickableRunestone.h"
#include "Portal.h"
#include "GrowingPlant.h"
#include "PickableSeed.h"
#include "BriarBush.h"
#include "PickableCristal.h"
#include "SmellableObject.h"
#include "HauntableReceptacle.h"
#include "PickableFragment.h"
#include "PickableKey.h"
#include "GameFramework/PlayerController.h" // PlayerInputComponent
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AJhada::AJhada() {
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	JhadaCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	JhadaCameraComponent->SetupAttachment(GetCapsuleComponent());

	JhadaCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 20 + BaseEyeHeight));
	JhadaCameraComponent->bUsePawnControlRotation = true;


	// Create a first person mesh component for the owning player.
	JhadaFPSMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	// Only the owning player sees this mesh.
	JhadaFPSMesh->SetOnlyOwnerSee(true);
	// Attach the FPS mesh to the FPS camera.
	JhadaFPSMesh->SetupAttachment(JhadaCameraComponent);
	// Disable some environmental shadowing to preserve the illusion of having a single mesh.
	JhadaFPSMesh->bCastDynamicShadow = false;
	JhadaFPSMesh->CastShadow = false;

	SmellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Smell Mesh"));
	SmellMesh->SetupAttachment(RootComponent);
	SmellMesh->SetOnlyOwnerSee(true);

	// The owning player doesn't see the regular (third-person) body mesh.
	GetMesh()->SetOwnerNoSee(true);

	HoldingComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HoldingComponent"));
	HoldingComponent->SetupAttachment(GetCapsuleComponent());
	HoldingComponent->SetRelativeLocation(FVector(50.0f, 20.0f, 60.0f));
	
	CurrentItem = nullptr;
	Portal = nullptr;
	GrowingPlant = nullptr;
	BriarBush = nullptr;
	Receptacle = nullptr;
	Smellable = nullptr;

	SetReplicates(true);
	SetReplicateMovement(true);

	//AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void AJhada::BeginPlay() {
	Super::BeginPlay();

	SmellMesh->SetVisibility(false);
	SmellMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SmellMesh->SetSimulatePhysics(false);

	Smellable->SetActorHiddenInGame(true);
}

// Called every frame
void AJhada::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	Start = JhadaCameraComponent->GetComponentLocation();
	ForwardVector = JhadaCameraComponent->GetForwardVector();
	End = (ForwardVector * 500.0f) + Start;

	if (!MovementInput.IsZero()) {
		//Scale our movement input axis values by 300 units per second
		MovementInput = MovementInput.GetSafeNormal() * 500.0f;
		FVector NewLocation = GetActorLocation();
		NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
		NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
		SetActorLocation(NewLocation);
	}

	// DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

	if (!bHoldingItem) {
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(APickableDoodad::StaticClass())) {
				CurrentItem = Cast<APickableDoodad>(Hit.GetActor());
			}
		} else {
			CurrentItem = nullptr;
			Portal = nullptr;
		}
	}

	if (bHoldingItem && CurrentItem->GetClass()->IsChildOf(APickableRunestone::StaticClass())) {
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(APortal::StaticClass())) {
				bPortal = true;
				Portal = Cast<APortal>(Hit.GetActor());
			} else {
				bPortal = false;
				Portal = nullptr;
			}
		}
	}

	if (bHoldingItem && CurrentItem->GetClass()->IsChildOf(APickableSeed::StaticClass())) {
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(AGrowingPlant::StaticClass())) {
				bGrowingPlant = true;
				GrowingPlant = Cast<AGrowingPlant>(Hit.GetActor());
			}
			else {
				bGrowingPlant = false;
				GrowingPlant = nullptr;
			}
		}
	}

	if (bHoldingItem && CurrentItem->GetClass()->IsChildOf(APickableCristal::StaticClass())) {
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(ABriarBush::StaticClass())) {
				bBriarBush = true;
				BriarBush = Cast<ABriarBush>(Hit.GetActor());
				//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Briar bush in front!"));
			}
			else {
				bBriarBush = false;
				BriarBush = nullptr;
			}
		}
	}

	if (bHoldingItem && (CurrentItem->GetClass()->IsChildOf(APickableKey::StaticClass()) || CurrentItem->GetClass()->IsChildOf(APickableFragment::StaticClass()))) {
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(AHauntableReceptacle::StaticClass())) {
				bReceptacle = true;
				Receptacle = Cast<AHauntableReceptacle>(Hit.GetActor());
			}
			else {
				bReceptacle = false;
				Receptacle = nullptr;
			}
		}
	}

	if (bPressedSmell) {
		Smellable->SetActorHiddenInGame(false);
		SmellMesh->SetVisibility(true);
		DisplaySmell();
	} else {
		SmellMesh->SetVisibility(false);
	}
	
}

// Called to bind functionality to input
void AJhada::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Set up "movement" bindings.
	PlayerInputComponent->BindAxis("MoveForward", this, &AJhada::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AJhada::MoveRight);
	// Set up "look" bindings.
	PlayerInputComponent->BindAxis("Turn", this, &AJhada::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AJhada::AddControllerPitchInput);

	// Set up "action" bindings.
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AJhada::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AJhada::StopJump);

	PlayerInputComponent->BindAction("Smell", IE_Pressed, this, &AJhada::StartSmelling);
	PlayerInputComponent->BindAction("Smell", IE_Released, this, &AJhada::StopSmelling);

	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AJhada::ToggleItemPickup);

}


void AJhada::MoveForward(float AxisValue) {
	AddMovementInput(AxisValue*GetActorForwardVector());
	//MovementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AJhada::MoveRight(float AxisValue) {
	AddMovementInput(AxisValue*GetActorRightVector());
	//MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AJhada::StartJump() {
	bPressedJump = true;
}

void AJhada::StopJump() {
	bPressedJump = false;
}


void AJhada::StartSmelling() {
	bPressedSmell = true;
}

void AJhada::StopSmelling() {
	bPressedSmell = false;
}

void AJhada::ToggleItemPickup() {
	if (CurrentItem && !bPortal && !bGrowingPlant && !bBriarBush) {
		bHoldingItem = !bHoldingItem;
		CurrentItem->PickUp();

		if (!bHoldingItem) {
			CurrentItem = nullptr;
		}
	} else if (CurrentItem && bPortal) {
		bHoldingItem = false;
		bPortal = false;
		Portal->PlaceRunestone(CurrentItem);
		CurrentItem = nullptr;
	} else if (CurrentItem && bGrowingPlant) {
		bHoldingItem = false;
		bGrowingPlant = false;
		GrowingPlant->PlaceSeed(CurrentItem);
		CurrentItem = nullptr;
	} else if (CurrentItem && bBriarBush) {
		BriarBush->MoveBush();
	} else if (CurrentItem && bReceptacle) {
		bHoldingItem = false;
		bReceptacle = false;
		Receptacle->Place(CurrentItem);
		CurrentItem = nullptr;
	}
}

void AJhada::DisplaySmell() {
	ISmellableObject* SmellTest = Cast<ISmellableObject>(Smellable);

	if (SmellTest) {

		FVector StartLocation = this->GetActorLocation();
		FVector EndLocation = Smellable->GetActorLocation();

		FVector Vector = EndLocation - StartLocation;
		Vector = Vector.GetSafeNormal() * 300;

		SmellMesh->SetWorldLocation(StartLocation + Vector);
	}
	else {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Not smelling anything !"));
		}
	}
}