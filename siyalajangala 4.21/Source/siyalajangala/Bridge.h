#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Bridge.generated.h"

class AHauntableDoodad;

UCLASS()
class SIYALAJANGALA_API ABridge : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABridge();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BridgeMesh;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlankMesh1;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlankMesh2;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlankMesh3;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void PlacePlank(AHauntableDoodad* Plank);
};
