// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HauntableDoodad.h"
#include "HauntableMovable.generated.h"

UCLASS()
class SIYALAJANGALA_API AHauntableMovable : public AHauntableDoodad
{
	GENERATED_BODY()
	
public:
	AHauntableMovable();

protected:
	virtual void BeginPlay() override;

	FVector2D MovementInput;

public:
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Action();

	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);

	virtual void Unpossess() override;
};
