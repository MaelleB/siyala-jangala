// Fill out your copyright notice in the Description page of Project Settings.


#include "Spirit.h"
#include "HauntableDoodad.h"
#include "SJ_PlayerController.h"
#include "DrawDebugHelpers.h"


// Sets default values
ASpirit::ASpirit() {
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a spring arm to give the camera smooth, natural-feeling motion.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	Capsule = GetCapsuleComponent();
	Capsule->SetupAttachment(RootComponent);
	Capsule->SetSimulatePhysics(true);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 100.0f), FRotator(-90.0f, 0.0f, 0.0f));
	SpringArm->TargetArmLength = 100.f;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 3.0f;

	// Create a camera and attach to our spring arm
    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ActualCamera"));
    Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	// Take control of the default player

	SetReplicates(true);
	SetReplicateMovement(true);
    // AutoPossessPlayer = EAutoReceiveInput::Player1;

	maxHeight = GetActorLocation().Z + 1000;
	minHeight = GetActorLocation().Z;

	CurrentItem = nullptr;
}

// Called when the game starts or when spawned
void ASpirit::BeginPlay() {
	Super::BeginPlay();

}

// Called every frame
void ASpirit::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	{
		if (bZoomingIn) {
			ZoomFactor += DeltaTime / 0.5f;         //Zoom in over half a second
		} else {
			ZoomFactor -= DeltaTime / 0.25f;        //Zoom out over a quarter of a second
		}
		ZoomFactor = FMath::Clamp<float>(ZoomFactor, 0.0f, 1.0f);
		//Blend our camera's FOV and our SpringArm's length based on ZoomFactor
		Camera->FieldOfView = FMath::Lerp<float>(90.0f, 60.0f, ZoomFactor);
		SpringArm->TargetArmLength = FMath::Lerp<float>(400.0f, 300.0f, ZoomFactor);
	}

	//Rotate our camera's pitch, but limit it so we're always looking downward
	{
		FRotator NewRotation = SpringArm->GetComponentRotation();
		NewRotation.Pitch = FMath::Clamp(NewRotation.Pitch + CameraInput.X, -80.0f, 0.0f);
		SpringArm->SetWorldRotation(NewRotation);
	}

	{
		if (!MovementInput.IsZero()) {
			//Scale our movement input axis values by 300 units per second
			MovementInput = MovementInput.GetSafeNormal() * 500.0f;
			FVector NewLocation = GetActorLocation();
			NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
			NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
			SetActorLocation(NewLocation);
		}
	}


	Start = Camera->GetComponentLocation();
	ForwardVector = Camera->GetForwardVector();
	End = (ForwardVector * 2000.0f) + Start;

	DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);
	if (!bPossessing) {
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(AHauntableDoodad::StaticClass())) {
				CurrentItem = Cast<AHauntableDoodad>(Hit.GetActor());
			}
		} else {
			CurrentItem = nullptr;
		}
	}
}

// Called to bind functionality to input
void ASpirit::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASpirit::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASpirit::MoveRight);
	// Set up "look" bindings.
	PlayerInputComponent->BindAxis("Turn", this, &ASpirit::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &ASpirit::PitchCamera);

	PlayerInputComponent->BindAction("ZoomIn", IE_Pressed, this, &ASpirit::ZoomIn);
	PlayerInputComponent->BindAction("ZoomIn", IE_Released, this, &ASpirit::ZoomOut);

	PlayerInputComponent->BindAction("GoUp", IE_Pressed, this, &ASpirit::GoUp);
	PlayerInputComponent->BindAction("GoDown", IE_Pressed, this, &ASpirit::GoDown);

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &ASpirit::Possess);
}


void ASpirit::MoveForward(float AxisValue) {
	MovementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void ASpirit::MoveRight(float AxisValue) {
	MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void ASpirit::PitchCamera(float AxisValue) {
	CameraInput.X = -1 * AxisValue;
}

void ASpirit::ZoomIn() {
	bZoomingIn = true;
}

void ASpirit::ZoomOut() {
	bZoomingIn = false;
}

void ASpirit::GoUp() {
	FVector NewLocation = GetActorLocation();
	NewLocation += GetActorUpVector() * 100;
	if (NewLocation.Z >= maxHeight) {
		NewLocation.Z = maxHeight;
	}
	SetActorLocation(NewLocation);
}

void ASpirit::GoDown() {
	FVector NewLocation = GetActorLocation();
	NewLocation += GetActorUpVector() * -100;
	if (NewLocation.Z <= minHeight) {
		NewLocation.Z = minHeight;
	}
	SetActorLocation(NewLocation);
}

void ASpirit::Possess() {
	if (CurrentItem) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Going to possess !"));
		}

		ASJ_PlayerController* PC = Cast<ASJ_PlayerController>(GetController());
		PC->PossessDoodad(CurrentItem);
		bPossessing = true;
	}
}

void ASpirit::setPossessing(bool p) {
	bPossessing = p;
	if (bPossessing == false) {
		CurrentItem = nullptr;
	}
}