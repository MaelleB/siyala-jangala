#pragma once

#include "CoreMinimal.h"
#include "HauntableDoodad.h"
#include "HauntableMovableReal.generated.h"

UCLASS()
class SIYALAJANGALA_API AHauntableMovableReal : public AHauntableDoodad
{
	GENERATED_BODY()

public:
	AHauntableMovableReal();

protected:
	virtual void BeginPlay() override;

	FVector2D MovementInput;
	float RotateInput;
	FRotator BaseRotation;
	FVector BasePosition;

public:
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Action();

	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);

	virtual void Unpossess() override;
};
