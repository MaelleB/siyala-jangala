#include "POrtalTriggerBox.h"

APortalTriggerBox::APortalTriggerBox() {
	//Register Events
	OnActorBeginOverlap.AddDynamic(this, &APortalTriggerBox::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &APortalTriggerBox::OnOverlapEnd);
}

// Called when the game starts or when spawned
void APortalTriggerBox::BeginPlay() {
	Super::BeginPlay();
}

void APortalTriggerBox::OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor)
{
	//if the overlapping actor is the specific actor we identified in the editor
	if (OtherActor && (OtherActor != this) && OtherActor == SpecificActor) {

	}
}

void APortalTriggerBox::OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor)
{
	//if the overlapping actor is the specific actor we identified in the editor
	if (OtherActor && (OtherActor != this) && OtherActor == SpecificActor) {

	}
}