// Fill out your copyright notice in the Description page of Project Settings.

#include "Portal.h"
#include "PickableDoodad.h"
#include "PickableRunestoneJhada.h"
#include "PickableRunestoneSpirit.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
APortal::APortal() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FrameMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Frame Mesh"));
	FrameMesh->SetupAttachment(RootComponent);
	//RootComponent = Cast<USceneComponent>(FrameMesh);

	SpiritRuneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Spirit rune Mesh"));
	SpiritRuneMesh->SetupAttachment(FrameMesh);

	JhadaRuneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Jhada rune Mesh"));
	JhadaRuneMesh->SetupAttachment(FrameMesh);

	PortalMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Portal Mesh"));
	PortalMesh->SetupAttachment(FrameMesh);
	PortalMesh->SetSimulatePhysics(false);
	PortalMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PortalMesh->SetGenerateOverlapEvents(false);

	isActivated = false;
}

// Called when the game starts or when spawned
void APortal::BeginPlay() {
	AActor::BeginPlay();
	FrameMesh->SetVisibility(true);
	SpiritRuneMesh->SetVisibility(false);
	JhadaRuneMesh->SetVisibility(false);
	PortalMesh->SetVisibility(false);
}

// Called every frame
void APortal::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);
}

void APortal::PlaceRunestone(APickableDoodad* Runestone) {
	if (Runestone->GetClass()->IsChildOf(APickableRunestoneJhada::StaticClass())) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Rune is Jhada's"));
		}
		JhadaRuneMesh->SetVisibility(true);
	}
	else if (Runestone->GetClass()->IsChildOf(APickableRunestoneSpirit::StaticClass())) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Rune is Spirit's"));
		}
		SpiritRuneMesh->SetVisibility(true);
	}
	Runestone->Destroy();

	if (JhadaRuneMesh->IsVisible() && SpiritRuneMesh->IsVisible()) {
		PortalMesh->SetVisibility(true);
		PortalMesh->SetGenerateOverlapEvents(true);
		isActivated = true;
	}
}