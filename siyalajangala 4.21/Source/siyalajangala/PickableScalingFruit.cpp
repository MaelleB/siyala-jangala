// Fill out your copyright notice in the Description page of Project Settings.

#include "PickableScalingFruit.h"

APickableScalingFruit::APickableScalingFruit() : APickableDoodad() {}

void APickableScalingFruit::BeginPlay() {
	Super::BeginPlay();

	Scale = GetActorScale3D();
}

void APickableScalingFruit::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bHolding && HoldingComp) {
		SetActorLocationAndRotation(HoldingComp->GetComponentLocation() + (PlayerCamera->GetForwardVector() * 10), HoldingComp->GetComponentRotation());
	}
}

void APickableScalingFruit::PickUp() {
	Super::PickUp();

	if (bHolding) {
		Jhada->SetActorScale3D(FVector(0.3, 0.3, 0.3));
		SetActorScale3D(Scale*0.3);
	}
	else {
		Jhada->SetActorScale3D(FVector(1, 1, 1));
		SetActorScale3D(Scale);
	}
}