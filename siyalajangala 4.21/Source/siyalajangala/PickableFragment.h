// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickableDoodad.h"
#include "PickableFragment.generated.h"

/**
 * 
 */
UCLASS()
class SIYALAJANGALA_API APickableFragment : public APickableDoodad
{
	GENERATED_BODY()
	
};
