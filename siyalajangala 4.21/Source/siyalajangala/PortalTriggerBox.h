#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "POrtalTriggerBox.generated.h"


UCLASS()
class SIYALAJANGALA_API APortalTriggerBox : public ATriggerBox {
	GENERATED_BODY()

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	// constructor sets default values for this actor's properties
	APortalTriggerBox();

	// overlap begin function
	UFUNCTION()
		void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	// overlap end function
	UFUNCTION()
		void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);

	// specific actor for overlap
	UPROPERTY(EditAnywhere)
		class AActor* SpecificActor;

};
