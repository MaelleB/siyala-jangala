#include "BriarBush.h"

// Sets default values
ABriarBush::ABriarBush() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BushMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bush Mesh 1"));
	BushMesh1->SetupAttachment(RootComponent);

	BushMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bush Mesh 2"));
	BushMesh2->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABriarBush::BeginPlay() {
	AActor::BeginPlay();
}

// Called every frame
void ABriarBush::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);

}

void ABriarBush::MoveBush() {
	if (bMoveOnce) {
		BushMesh1->SetWorldLocation(BushMesh1->GetComponentLocation() + MoveVector1);
		BushMesh2->SetWorldLocation(BushMesh2->GetComponentLocation() + MoveVector2);
		bMoveOnce = false;
	}
}