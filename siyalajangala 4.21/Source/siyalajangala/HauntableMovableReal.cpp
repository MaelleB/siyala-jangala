// Fill out your copyright notice in the Description page of Project Settings.

#include "HauntableMovableReal.h"

AHauntableMovableReal::AHauntableMovableReal() : AHauntableDoodad() {}

void AHauntableMovableReal::BeginPlay() {
	Super::BeginPlay();
	BaseRotation = GetActorRotation();
	BasePosition = GetActorLocation();
}

void AHauntableMovableReal::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (RotateInput != 0) {
		FVector Axis(0, 0, 1);

		//SetActorRotation(FQuat(Axis, RotateInput * DeltaTime));
		AddActorLocalRotation(FQuat(Axis, RotateInput * DeltaTime));
	}

	if (!MovementInput.IsZero()) {
		//Scale our movement input axis values by 300 units per second
		MovementInput = MovementInput.GetSafeNormal() * 500.0f;
		FVector NewLocation = GetActorLocation();
		NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
		NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
		SetActorLocation(NewLocation);
	}

}

void AHauntableMovableReal::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	APawn::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntableMovableReal::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AHauntableMovableReal::Action);

	PlayerInputComponent->BindAxis("MoveForward", this, &AHauntableMovableReal::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHauntableMovableReal::MoveRight);
}

void AHauntableMovableReal::Action() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Rotating to base rotation !"));
	}
	SetActorLocationAndRotation(BasePosition, BaseRotation);
}

void AHauntableMovableReal::MoveForward(float Value) {
	MovementInput.X = FMath::Clamp<float>(Value, -1.0f, 1.0f);
}

void AHauntableMovableReal::MoveRight(float Value) {
	RotateInput = FMath::Clamp<float>(Value, -1.0f, 1.0f);
}

void AHauntableMovableReal::Unpossess() {
	AHauntableDoodad::Unpossess();
	MovementInput = FVector2D(0, 0);
}