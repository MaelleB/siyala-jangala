#include "HauntablePlank.h"
#include "Bridge.h"
#include "SJ_PlayerController.h"
#include "DrawDebugHelpers.h"

AHauntablePlank::AHauntablePlank() : AHauntableDoodad() {
	maxHeight = GetActorLocation().Z + 1000;
	minHeight = GetActorLocation().Z;
	CurrentItem = nullptr;
	CameraInput.X = 0;
	CameraInput.Y = 0;
}

void AHauntablePlank::BeginPlay() {
	Super::BeginPlay();
}

void AHauntablePlank::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bPossessed) {
		//Rotate our camera's pitch, but limit it so we're always looking downward
		{
			FRotator NewRotation = SpringArm->GetComponentRotation();
			NewRotation.Pitch = FMath::Clamp(NewRotation.Pitch + CameraInput.X, -80.0f, 80.0f);
			SpringArm->SetWorldRotation(NewRotation);
		}

		if (CameraInput.Y != 0) {
			FVector Axis(0, 0, 1);
			AddActorLocalRotation(FQuat(Axis, CameraInput.Y * DeltaTime));
		}

		{
			if (!MovementInput.IsZero()) {
				//Scale our movement input axis values by 300 units per second
				MovementInput = MovementInput.GetSafeNormal() * 500.0f;
				FVector NewLocation = GetActorLocation();
				NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
				NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
				SetActorLocation(NewLocation);
			}
		}

		DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

		Start = Camera->GetComponentLocation();
		ForwardVector = Camera->GetForwardVector();
		End = (ForwardVector * 2000.0f) + Start;

		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(ABridge::StaticClass())) {
				CurrentItem = Cast<ABridge>(Hit.GetActor());
			}
			else {
				CurrentItem = nullptr;
			}
		}
		else {
			CurrentItem = nullptr;
		}
	}
}

void AHauntablePlank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	APawn::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntablePlank::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AHauntablePlank::Action);

	PlayerInputComponent->BindAxis("MoveForward", this, &AHauntablePlank::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHauntablePlank::MoveRight);

	PlayerInputComponent->BindAction("GoUp", IE_Pressed, this, &AHauntablePlank::GoUp);
	PlayerInputComponent->BindAction("GoDown", IE_Pressed, this, &AHauntablePlank::GoDown);

	PlayerInputComponent->BindAxis("Turn", this, &AHauntablePlank::YawCamera);
	PlayerInputComponent->BindAxis("LookUp", this, &AHauntablePlank::PitchCamera);
}

void AHauntablePlank::MoveForward(float AxisValue) {
	MovementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AHauntablePlank::MoveRight(float AxisValue) {
	MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AHauntablePlank::PitchCamera(float AxisValue) {
	CameraInput.X = -1 * AxisValue;
}

void AHauntablePlank::YawCamera(float AxisValue) {
	CameraInput.Y = AxisValue;
}

void AHauntablePlank::GoUp() {
	FVector NewLocation = GetActorLocation();
	NewLocation += GetActorUpVector() * 100;
	if (NewLocation.Z >= maxHeight) {
		NewLocation.Z = maxHeight;
	}
	SetActorLocation(NewLocation);
}

void AHauntablePlank::GoDown() {
	FVector NewLocation = GetActorLocation();
	NewLocation += GetActorUpVector() * -100;
	if (NewLocation.Z <= minHeight) {
		NewLocation.Z = minHeight;
	}
	SetActorLocation(NewLocation);
}


void AHauntablePlank::Action() {
	if (CurrentItem != nullptr) {
		Unpossess();
		CurrentItem->PlacePlank(this);
	}
}

void AHauntablePlank::Unpossess() {
	AHauntableDoodad::Unpossess();
	MovementInput = FVector2D(0, 0);
}