#pragma once

#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Jhada.generated.h"

UCLASS()
class SIYALAJANGALA_API AJhada : public ACharacter {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		class USceneComponent* HoldingComponent;
	// FPS camera.
	UPROPERTY(VisibleAnywhere)
		UCameraComponent* JhadaCameraComponent;
	// First-person mesh (arms), visible only to the owning player.
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* JhadaFPSMesh;
	UPROPERTY(EditAnywhere, Category = Mesh)
		UStaticMeshComponent* SmellMesh;

public:
	// Sets default values for this character's properties
	AJhada();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector2D MovementInput;

	bool bPressedSmell = false;
	bool bHoldingItem;
	FVector HoldingComp;
	FVector Start;
	FVector ForwardVector;
	FVector End;
	FHitResult Hit;
	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParams;

	bool bPortal;
	bool bGrowingPlant;
	bool bBriarBush;
	bool bReceptacle;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Handles input for moving forward and backward.
	UFUNCTION()
	void MoveForward(float Value);

	// Handles input for moving right and left.
	UFUNCTION()
	void MoveRight(float Value);

	// Sets jump flag when key is pressed.
	UFUNCTION()
	void StartJump();

	// Clears jump flag when key is released.
	UFUNCTION()
	void StopJump();

	UFUNCTION()
	void StartSmelling();

	UFUNCTION()
	void StopSmelling();

	class APickableDoodad* CurrentItem;
	class APortal* Portal;
	class AGrowingPlant* GrowingPlant;
	class ABriarBush* BriarBush;
	class AHauntableReceptacle* Receptacle;
	
	UPROPERTY(EditAnywhere)
		AActor* Smellable;

public:
	void ToggleItemPickup();
	void DisplaySmell();

};
