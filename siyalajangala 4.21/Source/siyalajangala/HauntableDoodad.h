#pragma once

#include "GameFramework/Pawn.h"
#include "Doodad.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "HauntableDoodad.generated.h"

UCLASS()
class SIYALAJANGALA_API AHauntableDoodad : public APawn, public IDoodad {
	GENERATED_BODY()

public:
	AHauntableDoodad();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Enum)
		ECorruptionState corruptionState;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* DoodadMesh;
	UPROPERTY(EditAnywhere)
		USpringArmComponent* SpringArm;
	UPROPERTY(EditAnywhere)
		UCameraComponent* Camera;

	bool bPossessed;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual ECorruptionState getCorruption() const override;
	virtual void setCorruption(ECorruptionState c_state) override;

	virtual void Unpossess();

	virtual void Action();

	void SetPossessed(bool p);
};
