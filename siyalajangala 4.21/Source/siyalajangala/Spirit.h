#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Spirit.generated.h"

class AHauntableDoodad;

UCLASS()
class SIYALAJANGALA_API ASpirit : public ACharacter {
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASpirit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		USpringArmComponent* SpringArm;
	UPROPERTY(EditAnywhere)
		UCameraComponent* Camera;

	UPROPERTY(EditAnywhere)
		UCapsuleComponent* Capsule;
	
	FVector2D MovementInput;
	FVector2D CameraInput;
	float ZoomFactor;
	bool bZoomingIn;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		float minHeight;
	UPROPERTY(EditAnywhere, Category = Gameplay)
		float maxHeight;

	AHauntableDoodad* CurrentItem;
	bool bPossessing = false;

	// To create the Hit vector, to see what actor we are aiming for
	FVector Start;
	FVector ForwardVector;
	FVector End;
	FHitResult Hit;
	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParams;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Handles input for moving forward and backward.
	void MoveForward(float AxisValue);
	// Handles input for moving right and left.
	void MoveRight(float AxisValue);
	// Rotate camera up and down
	void PitchCamera(float AxisValue);

	void ZoomIn();
	void ZoomOut();

	void GoUp();
	void GoDown();

	void Possess();

	void setPossessing(bool p);
};
