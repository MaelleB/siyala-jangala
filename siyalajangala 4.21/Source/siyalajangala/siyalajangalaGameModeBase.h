// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "siyalajangalaGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class SIYALAJANGALA_API AsiyalajangalaGameModeBase : public AGameMode
{
	GENERATED_BODY()
	virtual void StartPlay() override;



};
