#include "GrowingPlant.h"
#include "PickableDoodad.h"
#include "PickableSeed.h"
#include "HauntableDoodad.h"
#include "HauntableWater.h"

// Sets default values
AGrowingPlant::AGrowingPlant() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	EarthMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Earth Mesh"));
	//EarthMesh->SetupAttachment(RootComponent);
	RootComponent = EarthMesh;

	SeedMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Seed Mesh"));
	SeedMesh->SetupAttachment(RootComponent);
	
	WaterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Water Mesh"));
	WaterMesh->SetupAttachment(RootComponent);
	
	PlantMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 1"));
	PlantMesh1->SetupAttachment(RootComponent);
	PlantMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 2"));
	PlantMesh2->SetupAttachment(RootComponent);
	PlantMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 3"));
	PlantMesh3->SetupAttachment(RootComponent);
	PlantMesh4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 4"));
	PlantMesh4->SetupAttachment(RootComponent);
	PlantMesh5 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 5"));
	PlantMesh5->SetupAttachment(RootComponent);
	
	bSeed = false;
	bWater = false;
}

// Called when the game starts or when spawned
void AGrowingPlant::BeginPlay() {
	AActor::BeginPlay();
	
	EarthMesh->SetVisibility(true);
	SeedMesh->SetVisibility(false);
	SeedMesh->SetSimulatePhysics(false);
	SeedMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WaterMesh->SetVisibility(false);
	WaterMesh->SetSimulatePhysics(false);
	WaterMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh1->SetVisibility(false);
	PlantMesh1->SetSimulatePhysics(false);
	PlantMesh1->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh2->SetVisibility(false);
	PlantMesh2->SetSimulatePhysics(false);
	PlantMesh2->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh3->SetVisibility(false);
	PlantMesh3->SetSimulatePhysics(false);
	PlantMesh3->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh4->SetVisibility(false);
	PlantMesh4->SetSimulatePhysics(false);
	PlantMesh4->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh5->SetVisibility(false);
	PlantMesh5->SetSimulatePhysics(false);
	PlantMesh5->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called every frame
void AGrowingPlant::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);

}

void AGrowingPlant::PlaceSeed(APickableDoodad* Seed) {
	bool isSeed = Seed->GetClass()->IsChildOf(APickableSeed::StaticClass());
	if (isSeed && !bWater && !bSeed) {
		SeedMesh->SetVisibility(true);
		bSeed = true;
	} else if (isSeed && bWater) {
		bSeed = true;
		GrowPlant();
	}

	Seed->Destroy();
}

void AGrowingPlant::PlaceWater(AHauntableDoodad* Water) {
	bool isWater = Water->GetClass()->IsChildOf(AHauntableWater::StaticClass());
	if (isWater && !bWater && !bSeed) {
		WaterMesh->SetVisibility(true);
		bWater = true;
	} else if (isWater && bSeed) {
		bWater = true;
		GrowPlant();
	}

	Water->Destroy();
}

void AGrowingPlant::GrowPlant() {
	if (bSeed && bWater) {
		SeedMesh->SetVisibility(false);
		WaterMesh->SetVisibility(false);

		PlantMesh1->SetVisibility(true);
		PlantMesh1->SetSimulatePhysics(false);
		PlantMesh1->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PlantMesh2->SetVisibility(true);
		PlantMesh2->SetSimulatePhysics(false);
		PlantMesh2->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PlantMesh3->SetVisibility(true);
		PlantMesh3->SetSimulatePhysics(false);
		PlantMesh3->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PlantMesh4->SetVisibility(true);
		PlantMesh4->SetSimulatePhysics(false);
		PlantMesh4->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PlantMesh5->SetVisibility(true);
		PlantMesh5->SetSimulatePhysics(false);
		PlantMesh5->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}
}