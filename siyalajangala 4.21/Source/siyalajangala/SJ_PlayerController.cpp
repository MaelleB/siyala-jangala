// Fill out your copyright notice in the Description page of Project Settings.

#include "SJ_PlayerController.h"

ASJ_PlayerController::ASJ_PlayerController() : APlayerController() {
	Spirit = nullptr;
	PossessedDoodad = nullptr;
	
	bReplicates = true;
	bReplicateMovement = true;
}

void ASJ_PlayerController::PossessDoodad(AHauntableDoodad* Doodad) {
	Spirit = Cast<ASpirit>(GetPawn());

	if (Spirit->GetClass()->IsChildOf(ASpirit::StaticClass()) && Doodad) {
		UnPossess();
		Possess(Doodad);
		PossessedDoodad = Doodad;
		PossessedDoodad->SetPossessed(true);
	} else {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Not a spirit / Not a doodad"));
		}
	}
}

void ASJ_PlayerController::UnpossessDoodad() {
	PossessedDoodad->SetPossessed(false);
	PossessedDoodad = nullptr;
	UnPossess();
	if (Spirit == nullptr) {
		return;
	}

	Possess(Spirit);
	Spirit->setPossessing(false);
	Spirit = nullptr;
}