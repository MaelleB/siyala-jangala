#pragma once

#include "ConcreteDoodad.h"
#include "Jhada.h"
#include "Camera/CameraComponent.h"
#include "PickableDoodad.generated.h"


UCLASS()
class SIYALAJANGALA_API APickableDoodad : public AConcreteDoodad {
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	APickableDoodad();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* DoodadMesh;

	UPROPERTY(EditAnywhere)
	USceneComponent* HoldingComp;

	UFUNCTION()
	virtual void PickUp();

	bool bHolding;
	bool bGravity;

	ACharacter* Jhada;
	UCameraComponent* PlayerCamera;
	FVector ForwardVector;

};
