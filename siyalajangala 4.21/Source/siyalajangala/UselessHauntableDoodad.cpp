// Fill out your copyright notice in the Description page of Project Settings.

#include "UselessHauntableDoodad.h"

AUselessHauntableDoodad::AUselessHauntableDoodad() : AHauntableDoodad() {}

void AUselessHauntableDoodad::BeginPlay() {
	Super::BeginPlay();
}

void AUselessHauntableDoodad::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void AUselessHauntableDoodad::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	APawn::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AUselessHauntableDoodad::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AUselessHauntableDoodad::MoveRight);
	// Set up "look" bindings.
	PlayerInputComponent->BindAxis("Turn", this, &AUselessHauntableDoodad::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AUselessHauntableDoodad::AddControllerPitchInput);

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntableDoodad::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AUselessHauntableDoodad::Action);
}

void AUselessHauntableDoodad::Action() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Possessing Useless !"));
	}
}

void AUselessHauntableDoodad::MoveForward(float Value) {
	// Find out which way is "forward" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void AUselessHauntableDoodad::MoveRight(float Value) {
	// Find out which way is "right" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(Direction, Value);
}