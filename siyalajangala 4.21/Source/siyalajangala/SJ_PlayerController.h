// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Spirit.h"
#include "HauntableDoodad.h"
#include "SJ_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SIYALAJANGALA_API ASJ_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ASJ_PlayerController();

protected:
	ASpirit* Spirit;
	AHauntableDoodad* PossessedDoodad;

public:
	void PossessDoodad(AHauntableDoodad* doodad);
	void UnpossessDoodad();
};
