// Fill out your copyright notice in the Description page of Project Settings.


#include "HauntableDoodad.h"
#include "SJ_PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"


AHauntableDoodad::AHauntableDoodad() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DoodadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoodadMesh"));
	DoodadMesh->SetSimulatePhysics(true);
	RootComponent = Cast<USceneComponent>(DoodadMesh);
	DoodadMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 20.0f), FRotator(45.0f, 0.0f, 0.0f));
	SpringArm->TargetArmLength = 30.f;
	SpringArm->bEnableCameraLag = false;
	SpringArm->CameraLagSpeed = 1.0f;

	// Create a camera and attach to our spring arm
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ActualCamera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
}

// Called when the game starts or when spawned
void AHauntableDoodad::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AHauntableDoodad::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AHauntableDoodad::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	APawn::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntableDoodad::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AHauntableDoodad::Action);

	PlayerInputComponent->BindAxis("Turn", this, &AHauntableDoodad::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AHauntableDoodad::AddControllerPitchInput);
}

ECorruptionState AHauntableDoodad::getCorruption() const {
	return corruptionState;
}

void AHauntableDoodad::setCorruption(ECorruptionState c_state) {
	corruptionState = c_state;
}

void AHauntableDoodad::Unpossess() {
	ASJ_PlayerController* PC = Cast<ASJ_PlayerController>(GetController());
	PC->UnpossessDoodad();
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Unpoossessing"));
	}
}

void AHauntableDoodad::Action() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Possessing Doodad"));
	}
}

void AHauntableDoodad::SetPossessed(bool p) {
	bPossessed = p;
}