#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "BriarBush.generated.h"

UCLASS()
class SIYALAJANGALA_API ABriarBush : public AActor {
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABriarBush();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BushMesh1;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BushMesh2;

	UPROPERTY(EditAnywhere)
		FVector MoveVector1;
	UPROPERTY(EditAnywhere)
		FVector MoveVector2;
	
	bool bMoveOnce = true;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void MoveBush();
};
