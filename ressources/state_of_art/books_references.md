﻿# Moteurs de jeux

Game Engines: Tools for Landscape Visualization and Planning?
Adrian HERWIG and Philip PAAR
https://bit.ly/2NGmIdh

## Unity

Unity Game Development Essentials
Will Goldstone
https://bit.ly/2pS1m3n

## Unreal Engine

An Introduction to Unreal Engine 4
Andrew Sanders

# Gameplay asymétrique

## Scare Tactics

https://scholarworks.rit.edu/cgi/viewcontent.cgi?referer=http://www.rit.edu/search/?q=asymetric&httpsredir=1&article=10221&context=theses

## Asymmetrical Gameplay : Gimmick or Revolution ?

http://www.theoryofgaming.com/asymmetrical-gameplay-gimmick-or-revolution/

## Evolve

https://en.wikipedia.org/wiki/Evolve_(video_game)

*Players are split in 2 groups : the monster and the team who wants to defeat it.*

## Witch It !

http://www.witchit.com/

## Blind Trust

http://www.blindtrustgame.com/

One player is deaf, the other one is blind

## Hacktag

http://www.hacktag-thegame.com/

One player is a secret agent, the other is a hacker who controls a virus

# Game Design

Level up! The guide to great video game design
Scott Roger

Complete Kobold Guide to Game Design 
Wolfgang Baur

Theory of Fun Game Design
Raph Koster

Différents livres puzzle-design : http://www.scottkim.com.previewc40.carrierzone.com/thinkinggames/
