# Généralités

## Ambiance du jeu

On doit voir une progression entre les niveaux. Cette progression doit se voir notamment dans la quantité de corruption. Elle devrait également se remarquer dans le rythme et l'ambiance de la musique, afin de donner une réelle impression de s'enfoncer dans un environnement plus maléfique. Dans le niveau 4, l'arbre mère doit évidemment être présent. 

## Progression du gameplay

Comme suggéré dans les livres de game design, il faudrait éviter d'introduire toutes les mécaniques de jeu dès le premier niveau. Il serait pertinent, en rapport avec la progression de l'environnement, de ne mettre la purification des entités que plus tard dans le jeu par exemple. Le début du jeu devrait servir à comprendre comment fonctionnent les énigmes de façon relativement simple (chercher des objets/endroits qui permettent des actions spécifiques et faire le lien). Les mécaniques plus "complexes" seront introduites par la suite (flairer/posséder un objet devraient être introduites à la fin du niveau 1 ou au début du niveau 2 par exemple).
