Level design

Enigmes - Niveau 1 - Bordure de la for�t

Pile de rochers trop lourds pour Jhada, l'esprit doit les poss�der pour les d�placer/faire tomber la pile pour r�cup�rer un objet au sommet. -> fruit qui permet de r�tr�cir pour passer dans un passage �troit ? 
La pile de rocher contient une pierre avec une rune dessus.
Et un fruit qui permet de r�tr�cir pour aller chercher une pierre runique (dans un endroit inaccessible � Jhada avec sa taille normale). -> trou dans lequel Jhada trouve la pierre -> l'esprit poss�de et d�place une racine pour qu'elle remonte et retour par au-dessus de l'obstacle
Fin du niveau gr�ce � un portail bloqu� qui doit se d�sactiver avec deux runes.

Rochers trop lourds pour Jhada (les gros) puis les petits que Jhada peut porter.

Portail (magique) qui se d�bloquerait gr�ce aux pierres qui ont �t� r�cup�r�es.

Sch�ma explicatif :

OOf-----------------XXXX_                    _

        |               %

        |__r__%


Objets : 
HauntableDoodads : branche, rochers
PickableDoodads : petites pierres, objet magique r�tr�cissant, pierres runiques
Autre : portail magique

Sp�cificit�s du d�cor : Corruption tr�s l�g�re

    
Enigmes - Niveau 2 - Rivi�re

- Pont cass� � reconstruire
- Jhada plante une graine qui sent fort, l'esprit poss�de quelque chose qui am�ne de l'eau � la graine et la fait ensuite pousser en une fleur qui sent fort -> intro au flair
- Cristaux de l'arbre m�re � amener � un endroit qui bloque le chemin -> poussent vers fleurs qui sentent fort 


Sp�cificit�s du d�cor : une rivi�re/un gouffre � franchir. Corruption un peu plus pr�sente.

Enigmes - Niveau 3 - Coeur de la for�t

- Environnement sombre avec n�cessit� de la lanterne. Lanterne qui a des plantes corrompues qui la bloquent -> intro � la purification
- Environnement qui s'�claire au fur et � mesure que la corruption est purifi�e.

Sp�cificit�s du d�cor : for�t dense. Corruption tr�s pr�sente. 

Enigmes - Niveau 4 - Arbre-m�re

Ce niveau doit �tre difficile. Il faut donc des �nigmes en plusieurs �tapes pour rejoindre l'arbre m�re. Il doit �galement permettre d'utiliser toutes les m�caniques : possession par l'esprit, flair pour trouver des objets et purification d'entit�s. 

Sp�cificit�s du d�cor : l'arbre m�re (je vois bien un arbre qui a des cristaux lumineux) 

M�canismes de guidage

Id�es :
    - Chemin clair
    - Brume mouvante
    
Enigmes diverses

- Ramener ses oeufs � un oiseau qui barre le passage. -> flair

