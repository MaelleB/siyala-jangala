# Niveau 1

Ce niveau sera une introduction au jeu et permettra aux joueurs de s'habituer aux contrôles et au fonctionnement des énigmes.

## Ambiance du niveau

Le niveau doit présenter assez peu de corruption, de façon à ce que le joueur comprenne tout de même que quelque chose ne va pas dans la forêt, mais que cela reste discret. En effet, si cela était évident, Jhada n'aurait pas eu besoin que l'esprit vienne lui annoncer le problème. La musique devrait de préférence être relativement calme.

Ce niveau doit donc comporter de la végétation saine, seuls quelques arbres/arbustes présenteront des traces de corruption.

## Mécaniques à introduire

Les énigmes de ce niveau ne doivent pas être trop complexes. Les joueurs doivent s'habituer aux contrôles et interactions. Des aides contextuelles supplémentaires devraient être ajoutées la première fois qu'un joueur doit utiliser une mécanique spécifique.

Il faudrait donc limiter les mécaniques à la recherche et utilisation d'objets et actions spécifiques à ces objets. 
